#!/usr/bin/env python3

import time
import datetime as dt
import serial
import numpy as np

# constants
zero_K = 273.15
a=1e4
b=3435
d=25

def temp_from_R(R):
  # values
  e=d+zero_K
  c=a*np.exp(-b/e)
  f=b/np.log(R/c)
  g=f-zero_K
  return g


ser = serial.Serial('/dev/ttyUSB1')  # open serial port
# Open a file with access mode 'a'
outfile = open('tempLog.csv', 'a')

while (True):
  ser.write(b'FETC?\n')
  myLine = ser.readline() 
  resistance=float(myLine)
  g=temp_from_R(resistance)
  myTime = dt.datetime.now()
  print("\n%s\t%f" % (myTime, g), end="")
  outfile.write("%s, %f, %f\n" % (myTime, g, myTime.timestamp()))
  outfile.flush()
  time.sleep(1)



