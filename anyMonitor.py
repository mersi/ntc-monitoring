#!/usr/bin/env python3

import time
import datetime as dt
import serial
import numpy as np

ser = serial.Serial('/dev/ttyUSB0')  # open serial port
# Open a file with access mode 'a'

while (True):
  ser.write(b'FETC?\n')
  myLine = ser.readline() 
  value=float(myLine)
  myTime = dt.datetime.now()
  print("\n%s\t%f" % (myTime, value), end="")
  time.sleep(1)



